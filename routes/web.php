<?php

use App\Http\Controllers\Auth\LoginController;
use App\Http\Controllers\Auth\RegisterController;
use App\Http\Controllers\IndexController;

Route::get('/', [IndexController::class, 'index']);

Route::get('/login', [LoginController::class, 'showLoginForm'])->name('login-form');
Route::post('/login', [LoginController::class, 'login'])->name('login');

Route::get('/sign-up', [RegisterController::class, 'showSignUpForm'])->name('sign-up-form');
Route::post('/sign-up', [RegisterController::class, 'signUp'])->name('sign-up');

Route::get('/logout', [IndexController::class, 'logout'])->name('logout');
