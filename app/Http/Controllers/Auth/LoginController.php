<?php

namespace App\Http\Controllers\Auth;

use App\Auth0User;
use App\Http\Controllers\Controller;
use Auth0\SDK\API\Authentication;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showLoginForm()
    {
        return view('login');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Auth0\SDK\Exception\ApiException
     * @throws \Illuminate\Validation\ValidationException
     */
    public function login(Request $request)
    {
        $validated = $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required',
        ]);

        /** @var \Auth0\SDK\Auth0 $auth0 */
        $auth0 = app('auth0');

        try {
            $auth = new Authentication(
                config('auth0.domain'),
                config('auth0.client_id'),
                config('auth0.client_secret'),
                config('auth0.audience'),
                'openid'
            );

            $oauthToken = $auth->oauth_token([
                'grant_type' => 'password',
                'username'   => $validated['email'],
                'password'   => $validated['password']
            ]);

            $user = $auth->userinfo($oauthToken['access_token']);

            $auth0->setUser($user)->setAccessToken($oauthToken['access_token']);

            Auth::login(new Auth0User($user, $oauthToken['access_token']));

            return back();
        } catch (ClientException $exception) {
            $response = (string) $exception->getResponse()->getBody();

            $info = collect(json_decode($response, true));
            return back()->withErrors($info->only(['error_description']))->withInput();
        }
    }
}
