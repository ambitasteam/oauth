<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\User;
use Auth0\SDK\API\Authentication;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function showSignUpForm()
    {
        return view('sign-up');
    }

    /**
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse
     * @throws \Illuminate\Validation\ValidationException
     * @throws \Auth0\SDK\Exception\ApiException
     */
    public function signUp(Request $request)
    {
        $validated = $this->validate($request, [
            'email'    => 'required|email',
            'password' => 'required|confirmed',
        ]);

        try {
            $auth = new Authentication(
                config('auth0.domain'),
                config('auth0.client_id'),
                config('auth0.client_secret'),
                config('auth0.audience'),
                'openid'
            );

            $auth->dbconnections_signup(
                $validated['email'],
                $validated['password'],
                'Username-Password-Authentication'
            );

            return redirect()->route('login-form')->with('message', 'You have been successfully registered. Please sign in.')->withInput();
        } catch (ClientException $exception) {
            $response = (string) $exception->getResponse()->getBody();

            $info = collect(json_decode($response, true));
            if ('invalid_signup' === $info->get('code')) {
                $message = ['Account already registered'];
            } elseif ('invalid_password' === $info->get('code')) {
                $rules = collect(preg_split('/\\n/', $info->get('policy'), -1, PREG_SPLIT_NO_EMPTY));

                $message = array_merge([$info->get('message')], $rules->toArray());
            } else {
                $message = [$info->get('description')];
            }

            return back()->withErrors($message)->withInput();
        }
    }
}
