<?php

namespace App;

use Illuminate\Contracts\Auth\Authenticatable;

class Auth0User implements Authenticatable
{
    /**
     * @var array
     */
    private $userinfo;
    private $accessToken;

    /**
     * Auth0User constructor.
     *
     * @param array $userinfo
     * @param $accessToken
     */
    public function __construct(array $userinfo, $accessToken)
    {
        $this->userinfo    = $userinfo;
        $this->accessToken = $accessToken;
    }

    /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName()
    {
        return 'id';
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this->userinfo['sub'];
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->accessToken;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param string $value
     *
     * @return void
     */
    public function setRememberToken($value)
    {
    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return void
     */
    public function getRememberTokenName()
    {
    }

    /**
     * Add a generic getter to get all the properties of the userInfo.
     *
     * @return mixed the related value or null if it is not set
     */
    public function __get($key)
    {
        if (!array_key_exists($key, $this->userinfo)) {
            return null;
        }
        return $this->userinfo[$key];
    }
}
