<?php

namespace App\Providers;

use App\Auth0User;
use Illuminate\Contracts\Auth\Authenticatable;
use Illuminate\Contracts\Auth\UserProvider;

class Auth0UserProvider implements UserProvider
{
    private function user($user, $accessToken)
    {
        return new Auth0User($user, $accessToken);
    }

    /**
     * @param array $credentials
     *
     * @return \App\Auth0User|\Illuminate\Contracts\Auth\Authenticatable|null
     * @throws \Auth0\SDK\Exception\ApiException
     * @throws \Auth0\SDK\Exception\CoreException
     */
    public function retrieveByCredentials(array $credentials)
    {
        /** @var \Auth0\SDK\Auth0 $auth0 */
        $auth0 = app('auth0');

        return $this->user($auth0->getUser(), $auth0->getAccessToken());
    }

    /**
     * @param mixed $identifier
     * @param string $token
     *
     * @return \App\Auth0User|\Illuminate\Contracts\Auth\Authenticatable|null
     * @throws \Auth0\SDK\Exception\ApiException
     * @throws \Auth0\SDK\Exception\CoreException
     */
    public function retrieveByToken($identifier, $token)
    {
        /** @var \Auth0\SDK\Auth0 $auth0 */
        $auth0 = app('auth0');

        return $this->user($auth0->getUser(), $auth0->getAccessToken());
    }

    /**
     * @param mixed $identifier
     *
     * @return \App\Auth0User|\Illuminate\Contracts\Auth\Authenticatable|null
     * @throws \Auth0\SDK\Exception\ApiException
     * @throws \Auth0\SDK\Exception\CoreException
     */
    public function retrieveById($identifier)
    {
        /** @var \Auth0\SDK\Auth0 $auth0 */
        $auth0 = app('auth0');

        return $this->user($auth0->getUser(), $auth0->getAccessToken());
    }

    /**
     * Update the "remember me" token for the given user in storage.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param string $token
     *
     * @return void
     */
    public function updateRememberToken(Authenticatable $user, $token)
    {
    }

    /**
     * Validate a user against the given credentials.
     *
     * @param \Illuminate\Contracts\Auth\Authenticatable $user
     * @param array $credentials
     *
     * @return bool
     */
    public function validateCredentials(Authenticatable $user, array $credentials)
    {
        return null;
    }
}
