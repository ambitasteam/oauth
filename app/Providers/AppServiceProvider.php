<?php

namespace App\Providers;

use Auth0\SDK\Auth0;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('auth0', function () {
            return new Auth0([
                'domain'               => config('auth0.domain'),
                'client_id'            => config('auth0.client_id'),
                'client_secret'        => config('auth0.client_secret'),
                'redirect_uri'         => config('auth0.redirect_uri', '  '),
                'persist_access_token' => true,
            ]);
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Auth::provider('auth0', function () {
            return new Auth0UserProvider;
        });
    }
}
