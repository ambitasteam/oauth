@extends('layout')

@section('content')


    @if(auth()->check())
        <div id="user">
            @php $user = auth()->user() @endphp
            <div>
                <img src="{{ $user->picture }}" alt="{{ $user->nickname }}">
            </div>
            Welcome {{ $user->nickname }} <br>
            {{ $user->email }} <br>
            Your email/account <strong>@if($user->email_verified) is @else is not @endif</strong> verified<br>
            <a href="{{ route('logout') }}">Logout</a>
        </div>
    @else
        <div>
            Please, <a href="{{ route('login-form') }}">sign in</a> or <a href="{{ route('sign-up-form') }}">sign up</a>
        </div>
    @endif

@endsection
