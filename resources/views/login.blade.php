@extends('layout')

@section('content')
    <div id="login-form">
        @if (session('message'))
            <div class="text-green">
                {{ session('message') }}
            </div>
        @endif
        @if ($errors->any())
            <div class="text-red">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('login') }}" method="post">
            @csrf

            <label for="email">E-mail</label>
            <input type="text" name="email" id="email" value="{{ old('email') }}">

            <label for="password">Password</label>
            <input type="password" name="password" id="password">

            <button type="submit">Login</button>
        </form>
        <a href="{{ route('sign-up-form') }}">sign up</a>
    </div>
@endsection
