@extends('layout')

@section('content')
    <div id="login-form">
        @if (session('message'))
            <div class="test-green">
                {{ session('message') }}
            </div>
        @endif
        @if ($errors->any())
            <div class="text-red">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form action="{{ route('sign-up') }}" method="post">
            @csrf

            <label for="email">E-mail</label>
            <input type="text" name="email" id="email" value="{{ old('email') }}">

            <label for="password">Password</label>
            <input type="password" name="password" id="password">

            <label for="password_confirmation">Repeat password</label>
            <input type="password" name="password_confirmation" id="password_confirmation">

            <button type="submit">Sign up</button>
        </form>
        <a href="{{ route('login-form') }}">sign in</a>
    </div>
@endsection
