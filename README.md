## Setup

- Sign in to admin of oauth
- create app a new application (or use existing)
- Enable password auth
- create new (or use pre-existing) api
- update `AUTH0_*` configuration keys in your `.env` file
- see `routes/web.php` for available routes (and relevant code)

# Info

I chose to implement this in a clean laravel application for ease of use for
validation, redirecting logic, auth logic and ready-made dovenv support.

Password grant implemented because task stated to implement own form with 
username/email and password fields for a login form. This could be extended for
other supported types provided by auth0.

Application is built around password grant for signing in users.

Sign-up workflow is also provided
